<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create()->each(function ($user) {
        //     if ($user) {
        //         $post = \App\Models\Post::factory()->make();
        //         $user->posts()->save($post);
        //     }
        // });
        User::factory()
        ->count(10)
        ->hasPosts(1)
        ->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
