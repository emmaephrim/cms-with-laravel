<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $guarded = [];
    // protected $fillable = [
    //     'title',
    //     'body',
    //     'post_image',
    //     'user_id'
    // ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // public  function setPostImageAttribute($value)
    // {
    //     $this->attribute['post_image'] = asset($value);
    // }

    //     public function getPostImageAttribute($value)
    //     {
    //         return asset($value);
    //     }
}
