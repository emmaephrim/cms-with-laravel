<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Policies\PostPolicy;

class PostController extends Controller
{
    public  function index(Post $post)
    {
        // $posts = Post::all();
        // $posts = auth()->user()->posts()->paginate(2);
        $posts = auth()->user()->posts;
        return view("admin.posts.index", ['posts' => $posts]);
    }
    public static function show(Post $post)
    {
        // $user = User::findOrFail($post->user_id);
        return view("blog-post", ['post' => $post]);
    }

    public static function create()
    {
        return view("admin.posts.create");
    }

    public function store(Request $request)
    {
        // Auth::user();
        // dd(request()->all());
        $inputs = request()->validate([
            'title' => 'required | min:4 | max:255',
            'post_image' => 'file',
            'body' => 'required'
        ]);
        if (request('post_image')) {
            $inputs['post_image'] = request('post_image')->store('images');
            // $user = auth()->user();
            // $user->posts()->create($inputs);
            // return back();
        }
        $post = Post::create([
            'title' => $inputs['title'],
            'post_image' => $inputs['post_image'],
            'body' => $inputs['body'],
            'user_id' => auth()->user()->id,
        ]);
        session()->flash('post-created-message', 'Post was created');
        return redirect()->route('post.index');
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        $post->delete();
        Session::flash('message', "Post was deleted!");
        return back();
    }

    public function edit(Post $post)
    {
        $this->authorize('view', $post);
        return view('admin.posts.edit', ['post' => $post]);
    }

    public function update(Post $post, Request $request)
    {
        $inputs = request()->validate([
            'title' => 'required| min:4 | max:225',
            'post_image' => 'file',
            'body' => 'required'
        ]);
        if (request('post_image')) {
            $inputs['post_image'] = request('post_image')->store('images');
        }
        $this->authorize('update', $post);

        Session::flash('post-updated-message', 'Post Updated Successfully');
        DB::table('posts')->where('id', $post->id)->update($inputs);
        return redirect()->route('post.index');
    }
}
