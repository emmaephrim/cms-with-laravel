<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AdminsController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public static function index()
    {
        $user_id = auth()->user()->id;
        $user = User::findOrFail($user_id);
        // dd($user);

        return view('admin.index', ['user' => $user]);
    }
}
