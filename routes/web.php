<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminsController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get("/post/{post}", [PostController::class, 'show'])->name('post');

Route::middleware('auth')->group(function () {
    Route::get("/admin", [AdminsController::class, 'index'])->name('admin.index');

    Route::get("/admin/posts", [PostController::class, 'index'])->name('post.index');
    Route::get("/admin/posts/create", [PostController::class, 'create'])->name('post.create');
    Route::post("/admin/posts", [PostController::class, 'store'])->name('post.store');
    Route::delete("/admin/posts/{post}/destroy", [PostController::class, 'destroy'])->name('post.destroy');

    Route::get("/admin/posts/{post}/edit", [PostController::class, 'edit'])->name('post.edit');
    Route::patch("/admin/posts/{post}/update", [PostController::class, 'update'])->name('post.update');
});
