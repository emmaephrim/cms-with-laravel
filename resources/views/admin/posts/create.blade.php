<x-admin-master>
    @section('content')
        <h1>Create</h1>

        <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-group" id="title" placeholder="Enter Title" required
                    @error('title') is-invalid @enderror>
                @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="post_image">File</label>
                <input type="file" name="post_image" id="post_image" class="form-control-file" required>

            </div>

            <div class="form-group">
                <textarea name="body" id="body" class="form-control" cols="30" rows="10" required></textarea>
            </div>

            <button type="submit" class="btn btn-primary">
                {{-- {!! Form::submit($text, [$options]) !!} --}}
                Submit
            </button>

        </form>
    @endsection
</x-admin-master>
