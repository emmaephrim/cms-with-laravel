<x-admin-master>
    @section('content')
        <h1>Edit A Post</h1>

        <form action="{{ route('post.update', $post->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-group" id="title" placeholder="Enter Title"
                    value="{{ $post->title }}" required @error('title') is-invalid @enderror>
                @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="post_image">File</label>
                <div>
                    @if (!str_starts_with($post->post_image, 'https://'))
                        <img width="300px" src="{{ asset('storage/' . $post->post_image) }}" alt="">
                    @else
                        <img width="300px" src="{{ asset($post->post_image) }}" alt="">
                    @endif
                </div>
                <input type="file" name="post_image" id="post_image" class="form-control-file">

            </div>
            {{-- {{ dd($post) }} --}}
            <div class="form-group">
                <textarea name="body" id="body" class="form-control" cols="30" rows="10" required>{{ $post->body }}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">
                {{-- {!! Form::submit($text, [$options]) !!} --}}
                Update
            </button>

        </form>
    @endsection
</x-admin-master>
