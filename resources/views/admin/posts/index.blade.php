<x-admin-master>
    @section('content')
        <h1>All Posts</h1>
        @if (Session::has('message'))
            <div class="alert alert-danger">
                {{ Session::get('message') }}
            </div>
        @elseif (Session::has('post-created-message'))
            <div class="alert alert-success">
                {{ Session::get('post-created-message') }}
            </div>
        @elseif (Session::get('post-updated-message'))
            <div class="alert alert-success">
                {{ Session::get('post-updated-message') }}
            </div>
        @endif
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        {{-- <img src="{{ asset('storage/images/ygc5RzbapPIAKR5aTodsIqH0KT8wG4IQ03tt6bDz.png') }}" alt="Try"> --}}
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Owner</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Id</th>
                                <th>Owner</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($posts as $post)
                                <tr>
                                    <td> {{ $post->id }} </td>
                                    <td> {{ $post->user->name }} </td>
                                    <td> {{ $post->title }} </td>
                                    <td>
                                        @if (!str_starts_with($post->post_image, 'https://'))
                                            <img src="{{ asset('storage/' . $post->post_image) }}" width="150"
                                                alt="image">
                                        @else
                                            <img src="{{ asset($post->post_image) }}" width="150" alt="Image">
                                        @endif
                                    </td>
                                    <td> {{ $post->created_at->diffForHumans() }}</td>
                                    <td> {{ $post->updated_at->diffForHumans() }} </td>
                                    <td>
                                        @can('view', $post)
                                            <form action="{{ route('post.destroy', $post->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger" type="submit">Delete</button>
                                            </form>
                                        @endcan
                                    </td>
                                    <td>
                                        {{-- <form action="{{ route('post.edit', $post->id) }}" method="">
                                            @csrf
                                            @method('update') --}}
                                        @can('view', $post)
                                            <a href="{{ route('post.edit', $post->id) }}">
                                                <button class="btn btn-success" type="submit">
                                                    Edit</button>
                                            </a>
                                        @endcan
                                        {{-- </form> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{-- {{ $posts->links() }} --}}
    @endsection

    @section('scripts')
        <!-- Page level plugins -->
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

        <!-- Page level custom scripts -->
        <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
    @endsection
</x-admin-master>
